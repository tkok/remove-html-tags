#!/usr/bin/env python3
"""
Remove target html tags

This command can remove target html tags from opened html.
Target tags are assigned by command option like
`--targets=script --targets=style --targets="link"`
"""
import argparse

from bs4 import BeautifulSoup

PARSER = "lxml"

def get_args():
    """Get arguments by argparse

    Returns:
        argparse.Namespace (obj):
            class contains arguments set as option params
            
            obj.tags (list): Target tags
            obj.input_file (str): input html file
            obj.output_file (str): output html file
    """
    parser = argparse.ArgumentParser(
        prog="RemoveHtmlTags",
        description="""Command removes target html tags.
        Targets are assigned as args -t. 
        Command example:
            python main.py input.html output.html -t script -t link
    """)
    parser.add_argument(
        '-t','--tags', 
        action='append', 
        help='<Required> target tags', 
        required=True
        )
    parser.add_argument('input_file')
    parser.add_argument('output_file')
    return parser.parse_args()

def main():
    args = get_args()

    with open(args.input_file, "r") as f:
        bs = BeautifulSoup(f, PARSER)

    for tag in args.tags:
        [x.extract() for x in bs.find_all(tag)]

    with open(args.output_file, "w") as f:
        f.write(str(bs))
    

if __name__ == '__main__':
    main()