# remove-html-tags
Command line script removes html tags which targets are assigned as option params.

## Usage
`python main.py {input_file} {output_file} -t div -t img -t a`  
All tags `<div>` `<img>` `<a>` in input_file are removed and then save as output_file.
